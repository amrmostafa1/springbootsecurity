use amr;

SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `user` MODIFY COLUMN id bigint(20) auto_increment NOT NULL;



INSERT INTO `user`(
creation_date,
birth_date,
email,
mobile_number,
name,
password,
username
)
VALUES
(
now(),
'1984-11-21 03:00:02.0',
'email1@gmail.com',
'01111111111',
'Amr',
'$2a$10$EzRJqr49jaspYxYMXFTIcepac9oWq2SYY6jGzFLnlHvr17ymya446',
'user1'
),
(
now(),
'1970-01-01 03:00:02.0',
'email2@hotmail.com',
'01122222222',
'Mostafa',
'$2a$10$EzRJqr49jaspYxYMXFTIcepac9oWq2SYY6jGzFLnlHvr17ymya446',
'user2'
);

SET FOREIGN_KEY_CHECKS = 1;