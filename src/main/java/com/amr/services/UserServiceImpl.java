package com.amr.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.amr.etities.User;
import com.amr.repositories.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public List<User> getAllUsers() {
		return this.userRepository.findAll();
	}

	@Override
	public User getUser(Long id) {
		return this.userRepository.getOne(id);
	}

	@Override
	@Transactional
	public void saveUser(User user) {
		this.userRepository.save(user);
	}

	@Override
	public void updateUser(Long id, User user) {
		this.userRepository.save(user);
	}

	@Override
	public void deleteUser(Long id) {
		this.userRepository.deleteById(id);
	}

	@Override
	public User findByUsername(String username) {
		return this.userRepository.findByUsername(username);
	}
}