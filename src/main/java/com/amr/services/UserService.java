package com.amr.services;

import java.util.List;

import com.amr.etities.User;

public interface UserService {

	List<User> getAllUsers();

	User getUser(Long id);

	void saveUser(User user);

	void updateUser(Long id, User user);

	void deleteUser(Long id);

	User findByUsername(String username);
}