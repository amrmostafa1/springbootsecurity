package com.amr.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.amr.etities.User;
import com.amr.services.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@GetMapping("/users")
	public List<User> getAllUsers() {
		return this.userService.getAllUsers();
	}

	@GetMapping("/users/{id}")
	public User getUser(@PathVariable Long id) {
		return this.userService.getUser(id);
	}

	@PostMapping("/users")
	public String createUser(@RequestBody User user) {
		this.userService.saveUser(user);
		return "{\"success\": true, \"message\": User has been added successfully.}";
	}

	@PutMapping("/users/{id}")
	public String updateUser(@RequestBody User user, @PathVariable Long id) {
		this.userService.updateUser(id, user);
		return "{\"success\": true, \"message\": User has been updated successfully.}";
	}

	@DeleteMapping("/users/{id}")
	public String deleteUser(@PathVariable Long id) {
		this.userService.deleteUser(id);
		return "{\"success\": true, \"message\": User has been deleted successfully.}";
	}
}